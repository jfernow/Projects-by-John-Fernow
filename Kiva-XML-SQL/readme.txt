To view the project, you can either click kiva.ipynb (if you have Jupyter) or kiva.html (viewable in any browser).

In order to replicate the results of this program or make modifications, it is necessary to have an SQL server and import the same database and have a keychain for connecting to it.