To simply view the code of the project, you can view yelpAPI.py.

To view the process of developing it as well as the visualizations, navigate to the folder FullProject and click the corresponding .ipynb files (I recommend starting with Token_Acquisition, then Data_Acquisition, then Data_Analysis).

To view the .ipynb files, you can use Jupyter. If you do not have Jupyter installed, you can view the corresponding PDFs of them instead.

In order to replicate the results of this program or make modifications, it is necessary to have a Yelp developer account and a keychain for it.

Note, the program contains both Raw SQL and SQL Alchemy. The Raw SQL is never actually ran, but was just present to display that we understood it as well as SQL Alchemy. 