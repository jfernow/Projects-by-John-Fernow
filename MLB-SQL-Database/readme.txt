To simply view the code of the project, you can view mlbSQL.py.

To view the process of developing it as well as the visualizations, navigate to the folder FullProject and click the corresponding .ipynb files.

To view the .ipynb files, you can use Jupyter. If you do not have Jupyter installed, you can view the corresponding PDFs of them instead.

In order to replicate the results of this program or make modifications, it is necessary to have an SQL server and import the same database and have a keychain for connecting to it.
